const { app, BrowserWindow, globalShortcut, ipcMain } = require('electron')

function createWindow () {
    const win = new BrowserWindow({
        width: 600,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        },
        frame: false
    })

    win.loadFile('index.html')
    globalShortcut.register('Alt+numadd', () => {
        console.log('Alt+numadd was pressed')
        win.webContents.send('increment-count');
    })
    globalShortcut.register('Alt+numsub', () => {
        console.log('Alt+numsub was pressed')
        win.webContents.send('decrement-count');
    })
    globalShortcut.register('Alt+num0', () => {
        console.log('Alt+num0 was pressed')
        win.webContents.send('reset-count');
    })
}


app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
})

ipcMain.on('count-increase', (event, arg) => {
    console.log(arg);
    event.reply('count-increase-noted', 'noted')
})

